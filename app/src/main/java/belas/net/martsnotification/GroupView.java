package belas.net.martsnotification;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.ActionBar;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


public class GroupView extends BaseActivity implements AdapterView.OnItemClickListener {

    ArrayList<HashMap<String, String>> placesListItems = new ArrayList<HashMap<String, String>>();
    ArrayList<HashMap<String, String>> carsListItems = new ArrayList<HashMap<String, String>>();
    private static final String LOCATION_URL = Config.YOUR_SERVER_URL + "getFolders";
    private static final String GROUP_ID = "groupid";
    private static final String CAR_ID = "car_id";
    public static String KEY_REFERENCE = "reference"; // id of the place
    public static String KEY_NAME = "name"; // name of the place
    JSONParser jsonParser = new JSONParser();
    AlertDialogManager alert = new AlertDialogManager();
    String groupid = "";
    ListView lv;
    ProgressDialog pDialog;
    Context context = null;
    Controller aController = null;
    String deviceIMEI = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setSubtitle("Allowed Groups");

        setContentView(R.layout.activity_group_view);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        context = this;

        TelephonyManager tManager = (TelephonyManager) getBaseContext()
                .getSystemService(Context.TELEPHONY_SERVICE);
        deviceIMEI = tManager.getDeviceId();
        DBAdapter.getAllUserData();
        groupid = DBAdapter.getUserId(deviceIMEI);


        //groupid =  i.getStringExtra("groupid");

        // Place referece id
        //  groupid ="1582";
        // groupid = userData.get_userid();

        Log.i("gcm", " Userid  :::: " + groupid);
//        System.out.println("Group ID  ::::::::::::::::::::::: "+groupid);
        lv = (ListView) findViewById(R.id.list);


        if (lv == null) {
            System.out.println("List View not Created ");
            lv = (ListView) findViewById(R.id.list);
        } else {
            System.out.println("It is safe now");
        }
        //Lets load the group info and update the UI
        new sync().execute();


//        Intent in = new Intent(getBaseContext(), GetUpdates.class);
//        in.putExtra("userid",groupid);
//        startService(in);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Restore variables when coming from background
        if (deviceIMEI == null || groupid == null) {
            TelephonyManager tManager = (TelephonyManager) getBaseContext()
                    .getSystemService(Context.TELEPHONY_SERVICE);
            deviceIMEI = tManager.getDeviceId();
            DBAdapter.getAllUserData();
            groupid = DBAdapter.getUserId(deviceIMEI);
        }
        if (lv == null) {
            System.out.println("List View not Created ");
            lv = (ListView) findViewById(R.id.list);
        } else {
            System.out.println("It is safe now");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_group_view, menu);
        return true;
    }

    // Method to start the service


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String reference = ((TextView) view.findViewById(R.id.reference)).getText().toString();
        String plateNo = ((TextView) view.findViewById(R.id.name)).getText().toString();
        Intent in = new Intent(getApplicationContext(), ShowCars.class);
        System.out.println("" + reference + " " + reference + " ");
        in.putExtra("groupid", reference);
        in.putExtra("plateNo", plateNo);
        startActivity(in);
    }


    class sync extends AsyncTask<String, String, CarList> {


        protected void onPreExecute() {
            super.onPreExecute();
            if (pDialog == null) {
                pDialog = new ProgressDialog(GroupView.this);
                pDialog.setMessage(Html.fromHtml("<b>Please wait loading groups..."));
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(false);
            }

           pDialog.show();
        }

        @Override
        protected CarList doInBackground(String... arg0) {
            try {

                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("user_id", groupid));
                JSONObject json = jsonParser.makeHttpRequest(LOCATION_URL, "POST", params);
                System.out.println(json.toString());
                JSONObject jso = new JSONObject();
                CarList carlist = new CarList();
                String status = "";
                ArrayList<Car> list = new ArrayList<Car>();
                //						place
                //						list.status=json.getJSONObject("status");
                Car car = null;

                for (Iterator it = json.keys(); it.hasNext(); ) {
                    String key = (String) it.next();
//							                  System.out.println("Key "+key);
                    try {


                        car = new Car();
                        if (key.equalsIgnoreCase("status")) {
                            status = json.getString("status");
                        } else {


                            JSONObject jsonObject = json.getJSONObject(key);
                            String plateNo = jsonObject.getString("groupname");
                            String reference = jsonObject.getString("groupid");
//                                                      String ignition = jsonObject.getString("ignition");

                            car.setReference(plateNo);
//                                                      car.setIgnition(ignition);
                            car.setPlateNo(reference);
                            list.add(car);

//                                                      System.out.println("Date values  Car PlateNo" + plateNo + ", Reference " + reference );
                            //

//								db.addLocation(loc_name, dateinserted, longitude, latitude);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                carlist.setResults(list);
                carlist.setStatus(status);
                System.out.println("Status is  " + status);

                return carlist;

            } catch (Exception ex) {
                ex.printStackTrace();
                return null;
            }

        }

        protected void onPostExecute(final CarList carList) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {
                    if(carList!=null){
                        /**
                         * Updating parsed Places into LISTVIEW
                         * */
//					if()
                        // Get json response status
                        String status = carList.getStatus();
                        System.out.println("Status Code " + status);

                        // Check for all possible status
                        if (status.equalsIgnoreCase("OK")) {
                            // Successfully got places details
                            if (carList.getResults() != null) {
                                // loop through each place

                                List<Car> results = carList.getResults();
                                Iterator<Car> iterator = results.iterator();
                                //HashMap<String, String> map = new HashMap<String, String>();
                                Car car = null;
                                while (iterator.hasNext()) {
                                    HashMap<String, String> map = new HashMap<String, String>();
                                    car = (Car) iterator.next();
                                    map.put(KEY_REFERENCE, car.getPlateNo());
                                    map.put(KEY_NAME, car.getReference());
                                    placesListItems.add(map);
                                }
                                // list adapter
                                ListAdapter adapter = new SimpleAdapter(context, placesListItems,
                                        R.layout.group_list_item,
                                        new String[]{KEY_REFERENCE, KEY_NAME}, new int[]{
                                        R.id.reference, R.id.name});

                                //Add data to the spinner
                                lv.setAdapter(adapter);
                                lv.setOnItemClickListener(GroupView.this);
                            }
                        } else if (status.equals("ZERO_RESULTS")) {
                            // Zero results found
                            alert.showAlertDialog(context, "Near Places",
                                    "Sorry no places found. Try to change the types of places",
                                    false);
                        } else if (status.equals("UNKNOWN_ERROR")) {
                            alert.showAlertDialog(context, "Places Error",
                                    "Sorry unknown error occured.",
                                    false);
                        } else if (status.equals("OVER_QUERY_LIMIT")) {
                            alert.showAlertDialog(context, "Places Error",
                                    "Sorry query limit to google places is reached",
                                    false);
                        } else if (status.equals("REQUEST_DENIED")) {
                            alert.showAlertDialog(context, "Places Error",
                                    "Sorry error occured. Request is denied",
                                    false);
                        } else if (status.equals("INVALID_REQUEST")) {
                            alert.showAlertDialog(context, "Places Error",
                                    "Sorry error occured. Invalid Request",
                                    false);
                        } else {
                            alert.showAlertDialog(context, "Places Error",
                                    "Sorry error occured.",
                                    false);
                        }
                    }
                    else{
                        alert.showAlertDialog(context, "Connection Erro",
                                "Could not connect to server. Please try again later",
                                false);
                    }

                }
            });
        }


    }

}
