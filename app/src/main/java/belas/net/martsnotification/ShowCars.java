package belas.net.martsnotification;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.os.StrictMode;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import belas.net.martsnotification.adapter.CarListAdapter;


public class ShowCars extends BaseActivity {

    ArrayList<HashMap<String, String>> placesListItems = new ArrayList<HashMap<String, String>>();
    private static final String LOCATION_URL = Config.YOUR_SERVER_URL+"getLocations";
    private static final String GROUP_ID = "groupid";
    private static final String PLATE_NUM = "plateNo";
    private static final String CAR_ID = "car_id";
    public static String KEY_REFERENCE = "reference"; // id of the place
    public static String KEY_NAME = "name"; // name of the place
    public static String KEY_SPEED = "speed";
    JSONParser jsonParser = new JSONParser();
    AlertDialogManager alert = new AlertDialogManager();
    String groupid = "";
    String plateNo = "";
    ListView lv;
    ProgressDialog pDialog;
    TextView groupName;
    CarList carlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_cars);
        getSupportActionBar().setSubtitle("Vehicles available");

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        Intent intent = getIntent();

        // Place referece id
        groupid = intent.getStringExtra(GROUP_ID);
        System.out.println("Group ID  ::::::::::::::::::::::: "+groupid);

        plateNo = intent.getStringExtra("plateNo");
        groupName=(TextView)findViewById(R.id.group_name);
        groupName.setText(plateNo);
        lv = (ListView) findViewById(R.id.list);


        if (lv == null){
            System.out.println("List View not Created ");
            lv = (ListView) findViewById(R.id.list);
        }else{
            System.out.println("It is safe now");
        }

//        new sync().execute();
        new LoadPlaces().execute();

        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String reference = ((TextView) view.findViewById(R.id.reference)).getText().toString();
                String plateNo=  ((TextView) view.findViewById(R.id.name)).getText().toString();
                Intent in = new Intent(getApplicationContext(), MapsActivity.class);
                System.out.println("" + reference + " " + reference + " ");
                in.putExtra(CAR_ID, reference);
                in.putExtra("plateNo", plateNo);
                startActivity(in);
            }
        });

//        setContentView(R.layout.activity_show_cars);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (groupName == null)
            groupName = (TextView) findViewById(R.id.group_name);
        if (lv == null)
            lv = (ListView) findViewById(R.id.list);
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString(GROUP_ID, groupid);
        savedInstanceState.putString(PLATE_NUM, plateNo);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        groupid = savedInstanceState.getString(GROUP_ID);
        plateNo = savedInstanceState.getString(PLATE_NUM);

        groupName = (TextView) findViewById(R.id.group_name);
        groupName.setText(plateNo);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_show_cars, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {


            case R.id.action_settings:
                Intent in = new Intent(getApplicationContext(), MapsViewActivity.class);
                in.putExtra("groupid", groupid);
                startActivity(in);
                break;


        }
        return true;
    }


    class LoadPlaces extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ShowCars.this);
            pDialog.setMessage(Html.fromHtml("<b>Populating car list...</b>"));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting Places JSON
         */
        protected String doInBackground(String... args) {
            try {

                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("groupid", groupid));
                JSONObject json = jsonParser.makeHttpRequest(LOCATION_URL, "POST", params);
                System.out.println(json.toString());
                JSONObject jso = new JSONObject();
                carlist = new CarList();
                String status = "";
                ArrayList<Car> list = new ArrayList<Car>();
                //						place
                //						list.status=json.getJSONObject("status");
                Car car = null;

                for (Iterator it = json.keys(); it.hasNext(); ) {
                    String key = (String) it.next();
//							                  System.out.println("Key "+key);
                    try {


                        car = new Car();
                        if (key.equalsIgnoreCase("status")) {
                            status = json.getString("status");
                        } else {


                            JSONObject jsonObject = json.getJSONObject(key);
                            String plateNo = jsonObject.getString("car_name");
                            String reference = jsonObject.getString("carid");
//                                                      String ignition = jsonObject.getString("ignition");

                            car.setReference(reference);
//                                                      car.setIgnition(ignition);
                            car.setPlateNo(plateNo);

                            car.setSpeed(jsonObject.getString("speed"));
                            list.add(car);

//                                                      System.out.println("Date values  Car PlateNo" + plateNo + ", Reference " + reference );
                            //

//								db.addLocation(loc_name, dateinserted, longitude, latitude);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                carlist.setResults(list);
                carlist.setStatus(status);
                System.out.println("Status is  " + status);

                //						rowcount = db.getRowCount();
                //						System.out.println("Number of entries After insert "+rowcount);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }




        /**
         * After completing background task Dismiss the progress dialog
         * and show the data in UI
         * Always use runOnUiThread(new Runnable()) to update UI from background
         * thread, otherwise you will get error
         * *
         */
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            pDialog.dismiss();
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed Places into LISTVIEW
                     * */
//					if()
                    // Get json response status
                    String status = carlist.getStatus();
                    System.out.println("Status Code " + status);

                    // Check for all possible status
                    if (status.equalsIgnoreCase("OK")) {
                        // Successfully got places details
                        if (carlist.getResults() != null) {
                            // loop through each place

                            List<Car> results = carlist.getResults();
                            Iterator<Car> iterator = results.iterator();
                            //HashMap<String, String> map = new HashMap<String, String>();
                            Car car = null;
                            while (iterator.hasNext()) {
                                HashMap<String, String> map = new HashMap<String, String>();
                                car = (Car) iterator.next();
                                map.put(KEY_REFERENCE, car.getPlateNo());
                                map.put(KEY_NAME,  car.getReference());
                                map.put(KEY_SPEED, car.getSpeed());
                                placesListItems.add(map);
                            }
                            // list adapter
                            CarListAdapter adapter = new CarListAdapter(ShowCars.this, placesListItems,
                                    R.layout.car_list_item,
                                    new String[]{KEY_REFERENCE, KEY_NAME}, new int[]{
                                    R.id.name, R.id.reference});


                            // Adding data into listview
                            if (lv == null){
                                lv = (ListView) findViewById(R.id.list);
                            }
                            lv.setAdapter(adapter);
                        }
                    } else if (status.equals("ZERO_RESULTS")) {
                        // Zero results found
                        alert.showAlertDialog(ShowCars.this, "Near Places",
                                "Sorry no places found. Try to change the types of places",
                                false);
                    } else if (status.equals("UNKNOWN_ERROR")) {
                        alert.showAlertDialog(ShowCars.this, "Places Error",
                                "Sorry unknown error occured.",
                                false);
                    } else if (status.equals("OVER_QUERY_LIMIT")) {
                        alert.showAlertDialog(ShowCars.this, "Places Error",
                                "Sorry query limit to google places is reached",
                                false);
                    } else if (status.equals("REQUEST_DENIED")) {
                        alert.showAlertDialog(ShowCars.this, "Places Error",
                                "Sorry error occured. Request is denied",
                                false);
                    } else if (status.equals("INVALID_REQUEST")) {
                        alert.showAlertDialog(ShowCars.this, "Places Error",
                                "Sorry error occured. Invalid Request",
                                false);
                    } else {
                        alert.showAlertDialog(ShowCars.this, "Places Error",
                                "Sorry error occured.",
                                false);
                    }
                }
            });

        }

    }

    class sync extends AsyncTask<String, String, String> {


        protected void onPreExecute() {
//            super.onPreExecute();
//            pDialog = new ProgressDialog(ShowCars.this);
//            pDialog.setMessage(Html.fromHtml("<b>Please wait</b><br/>Loading Available Cars..."));
//            pDialog.setIndeterminate(false);
//            pDialog.setCancelable(false);
//            pDialog.show();
//        }
        }

        @Override
        protected String doInBackground(String... arg0) {
            return null;
        }

        protected void onPostExecute(String file_url) {
//			pDialog.dismiss();
            runOnUiThread(new Runnable() {
                              public void run() {
                                  try {

                                      List<NameValuePair> params = new ArrayList<NameValuePair>();
                                      params.add(new BasicNameValuePair("groupid", groupid));
                                      JSONObject json = jsonParser.makeHttpRequest(LOCATION_URL, "POST", params);
                                      System.out.println(json.toString());
                                      JSONObject jso = new JSONObject();
                                      carlist = new CarList();
                                      String status = "";
                                      ArrayList<Car> list = new ArrayList<Car>();
                                      //						place
                                      //						list.status=json.getJSONObject("status");
                                      Car car = null;

                                      for (Iterator it = json.keys(); it.hasNext(); ) {
                                          String key = (String) it.next();
//							                  System.out.println("Key "+key);
                                          try {


                                              car = new Car();
                                              if (key.equalsIgnoreCase("status")) {
                                                  status = json.getString("status");
                                              } else {


                                                  JSONObject jsonObject = json.getJSONObject(key);
                                                  String plateNo = jsonObject.getString("car_name");
                                                  String reference = jsonObject.getString("carid");
//                                                      String ignition = jsonObject.getString("ignition");

                                                  car.setReference(reference);
//                                                      car.setIgnition(ignition);
                                                  car.setPlateNo(plateNo);
                                                  list.add(car);

//                                                      System.out.println("Date values  Car PlateNo" + plateNo + ", Reference " + reference );
                                                  //

//								db.addLocation(loc_name, dateinserted, longitude, latitude);
                                              }
                                          }catch (Exception ex) {
                                              ex.printStackTrace();
                                          }
                                      }

                                      carlist.setResults(list);
                                      carlist.setStatus(status);
                                      System.out.println("Status is  " + status);

                                      //						rowcount = db.getRowCount();
                                      //						System.out.println("Number of entries After insert "+rowcount);

                                  } catch (Exception ex) {
                                      ex.printStackTrace();
                                  }
                              }
                          }
            );

        }


    }
}
