package belas.net.martsnotification;

/**
 * Created by David on 12/22/2014.
 */
public interface Config {


    // CONSTANTS
    static final String YOUR_SERVER_URL ="http://122.10.252.8:2301/";

    // Google project id
    static final String GOOGLE_SENDER_ID = "1008316388973";

    /**
     * Tag used on log messages.
     */
    static final String TAG = "GCM Marts Notification";

    static final String DISPLAY_MESSAGE_ACTION = "belas.net.martsnotification.gcm.DISPLAY_MESSAGE";

    static final String EXTRA_MESSAGE = "message";


}