package belas.net.martsnotification;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
import android.location.Address;


//import com.google.android.gms.wallet.Address;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


public class ViewDetails extends Activity {

    String longitude = "";
    String latitude = "";
    String speed = "";
    String mileage = "";
    String onoff = "";
    String time="";
    String fuel= "";
    String  car_id="";
    GUIStatics stats ;
    Context context;
    String plateNo;

    ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_details);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        Intent i = getIntent();
        context=this;

        // Place referece id
        car_id = i.getStringExtra("car_id");
        longitude = i.getStringExtra("longitude");
        latitude = i.getStringExtra("latitude");
        speed = i.getStringExtra("speed");
        mileage = i.getStringExtra("mileage");
        onoff = i.getStringExtra("status");
        time = i.getStringExtra("time");
        fuel = i.getStringExtra("fuel");
        plateNo=i.getStringExtra("plateNo");
        setTitle(" Plate No "+plateNo+" Current status "+speed);

        System.out.println("values "+car_id+" , "+longitude+" , "+latitude+" , "+speed+" , "+mileage+" , " + onoff+ " ," +fuel+","+time);
//        new GetLocation().execute(car_id, longitude, latitude, speed, mileage, onoff, fuel, time);


        TextView lbl_carid = (TextView) findViewById(R.id.cardidtxt);
        TextView lbl_address = (TextView) findViewById(R.id.addresstxt);
        TextView lbl_speed = (TextView) findViewById(R.id.speedtxt);
        TextView lbl_fuel = (TextView) findViewById(R.id.fueltxt);
        TextView lbl_status = (TextView) findViewById(R.id.statustext);
        TextView lbl_mileage = (TextView) findViewById(R.id.Mileagetxt);
        TextView lbl_time = (TextView) findViewById(R.id.timetxt);
//        TextView lbl_longitude = (TextView) findViewById(R.id.longitudetxt);
//        TextView lbl_latitude = (TextView) findViewById(R.id.latitidetxt);
        GUIStatics details =  getAddress(Double.parseDouble(latitude),Double.parseDouble(longitude));
        System.out.println("Details " +details.getCurrentAddress());
//
//        // Check for null data from google
//        // Sometimes place details might missing
        car_id = car_id == null ? "Not present" : car_id; // if name is null display as "Not present"
        speed = speed == null ? "Not present" : speed;
        onoff = onoff == null ? "Not present" : onoff;
        latitude = latitude == null ? "Not present" : latitude;
        longitude = longitude == null ? "Not present" : longitude;
        mileage = mileage == null ? "Not present" : mileage;
        time = time == null ? "Not present" : time;
        fuel = fuel == null ? "Not present" : fuel;

        lbl_carid.setText(plateNo);
//        lbl_latitude.setText(latitude);
//        lbl_longitude.setText(longitude);
        lbl_fuel.setText(fuel);
        lbl_speed.setText(speed);
        lbl_time.setText(time.replace('T',' '));
        lbl_address.setText(details.getCurrentAddress());
        lbl_mileage.setText(mileage);
        lbl_status.setText(onoff);




//
//        lbl_name.setText(name);
//        lbl_address

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_details, menu);
        return true;
    }

    public GUIStatics getAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        if (geocoder == null){
            System.out.println("Goecoder is null");
        }
        stats =  new GUIStatics();
        if (stats == null){
            System.out.println("stats  is null");
        }
        try {
            System.out.println("in get address lat "+lat+"  longitude "+lng);
            List<Address> addresses = geocoder.getFromLocation(lat/ 1000000.0, lng/ 1000000.0, 1);
            Address obj = addresses.get(0);
            String add = obj.getAddressLine(0);
            System.out.println("in get address "+add+" , "+obj.getSubAdminArea() + ","
                    + obj.getAdminArea());
            stats.setCurrentAddress( add
                    + ","
                    + obj.getAdminArea() +","+obj.getCountryName());
            stats.setLatitude(String.valueOf(obj.getLatitude()));
            stats.setLongitude(String.valueOf(obj.getLongitude()));
            stats.setCurrentCity(String.valueOf(obj.getSubAdminArea()));
            stats.setCurrentState(obj.getAdminArea());
            add = add + "\n" + obj.getCountryName();
            add = add + "\n" + obj.getCountryCode();
            add = add + "\n" + obj.getAdminArea();
            add = add + "\n" + obj.getPostalCode();
            add = add + "\n" + obj.getSubAdminArea();
            add = add + "\n" + obj.getLocality();
            add = add + "\n" + obj.getSubThoroughfare();

            Log.v("IGA", "Address" + add);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            System.out.println("Error "+e.getMessage());
            e.printStackTrace();
//                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }finally{
            return stats;
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class GetLocation extends AsyncTask<String, String, String> {


        protected void onPreExecute() {
            super.onPreExecute();
//            pDialog = new ProgressDialog(ShowCars.this);
//            pDialog.setMessage(Html.fromHtml("<b>Updating Database</b><br/>Loading Available Cars..."));
//            pDialog.setIndeterminate(false);
//            pDialog.setCancelable(false);
//            pDialog.show();
//        }
        }

        public GUIStatics getAddress(double lat, double lng) {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            stats =  new GUIStatics();
            try {
                System.out.println("in get address ");
                List<Address> addresses = geocoder.getFromLocation(lng, lat, 1);
                Address obj = addresses.get(0);
                String add = obj.getAddressLine(0);
                System.out.println("in get address "+add+" , "+obj.getSubAdminArea() + ","
                        + obj.getAdminArea());
                stats.setCurrentAddress( obj.getSubAdminArea() + ","
                        + obj.getAdminArea());
                stats.setLatitude(String.valueOf(obj.getLatitude()));
                stats.setLongitude(String.valueOf(obj.getLongitude()));
                stats.setCurrentCity(String.valueOf(obj.getSubAdminArea()));
                stats.setCurrentState(obj.getAdminArea());
                add = add + "\n" + obj.getCountryName();
                add = add + "\n" + obj.getCountryCode();
                add = add + "\n" + obj.getAdminArea();
                add = add + "\n" + obj.getPostalCode();
                add = add + "\n" + obj.getSubAdminArea();
                add = add + "\n" + obj.getLocality();
                add = add + "\n" + obj.getSubThoroughfare();

                Log.v("IGA", "Address" + add);

            } catch (IOException e) {
                // TODO Auto-generated catch block
                System.out.println("Error "+e.getMessage());
                e.printStackTrace();
//                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }finally{
                return stats;
            }
        }

        @Override
        protected String doInBackground(String... arg0) {
            return null;
        }

        protected void onPostExecute(String file_url) {

//			pDialog.dismiss();
            runOnUiThread(new Runnable() {
                              public void run() {
                                  try {
                                      TextView lbl_carid = (TextView) findViewById(R.id.cardidtxt);
                                      TextView lbl_address = (TextView) findViewById(R.id.addresstxt);
                                      TextView lbl_speed = (TextView) findViewById(R.id.speedtxt);
                                      TextView lbl_fuel = (TextView) findViewById(R.id.fueltxt);
                                      TextView lbl_status = (TextView) findViewById(R.id.statustext);
                                      TextView lbl_mileage = (TextView) findViewById(R.id.Mileagetxt);
                                      TextView lbl_time = (TextView) findViewById(R.id.timetxt);
//                                      TextView lbl_longitude = (TextView) findViewById(R.id.longitudetxt);
//                                      TextView lbl_latitude = (TextView) findViewById(R.id.latitidetxt);
                                      GUIStatics details =  getAddress(Double.parseDouble(latitude),Double.parseDouble(longitude));
                                      System.out.println("Details " + stats.getCurrentAddress());

                                      // Check for null data from google
                                      // Sometimes place details might missing
                                      car_id = car_id == null ? "Not present" : car_id; // if name is null display as "Not present"
                                      speed = speed == null ? "Not present" : speed;
                                      onoff = onoff == null ? "Not present" : onoff;
                                      latitude = latitude == null ? "Not present" : latitude;
                                      longitude = longitude == null ? "Not present" : longitude;
                                      mileage = mileage == null ? "Not present" : mileage;
                                      time = time == null ? "Not present" : time;
                                      fuel = fuel == null ? "Not present" : fuel;
                                  }catch (Exception exp){
                                      exp.printStackTrace();
                                  }

                              }


                          }
            );
        }
    }
}





