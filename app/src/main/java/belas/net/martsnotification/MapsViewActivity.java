package belas.net.martsnotification;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import org.json.JSONObject;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class MapsViewActivity extends FragmentActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private GoogleMap map; // Might be null if Google Play services APK is not available.
    MapView mapView;
    String  carid;
    CarLocation location;
    JSONParser jsonParser = new JSONParser();
    private static final String LOCATION_URL = Config.YOUR_SERVER_URL+"getLocations";
    private static final String GROUP_ID = "groupid";
    String groupid = "";
    LatLng CURRENT_LOC ;
    String plateNo="";
    private  final int BACKOFF_MILLI_SECONDS = 2000;
    private  final Random random = new Random();
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_view);
        context = this;

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        Intent i = getIntent();
        setTitle("Vehicles Available");

        // Place referece id
        groupid = i.getStringExtra(GROUP_ID);
        map = ((SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map)).getMap();
        new sync().execute();
//        setUpMapIfNeeded();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is
        // present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.menu_sethybrid:
                map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                break;
            case R.id.action_settings:
                Intent in = new Intent(getApplicationContext(), ViewDetails.class);
                in.putExtra("car_id", location.getCar_id());
                in.putExtra("plateNo",plateNo);
                in.putExtra("longitude",location.getLongitude());
                in.putExtra("latitude",location.getLatitude());
                in.putExtra("speed",location.getSpeed());
                in.putExtra("mileage",location.getMileage());
                in.putExtra("fuel",location.getFuel());
                in.putExtra("time",location.getTime());
                in.putExtra("status",location.getOnoff());
                startActivity(in);
                break;


        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
    }

    class sync extends AsyncTask<String, String, String> {


        protected void onPreExecute() {
            super.onPreExecute();
//            pDialog = new ProgressDialog(ShowCars.this);
//            pDialog.setMessage(Html.fromHtml("<b>Updating Database</b><br/>Loading Available Cars..."));
//            pDialog.setIndeterminate(false);
//            pDialog.setCancelable(false);
//            pDialog.show();
//        }
        }

        @Override
        protected String doInBackground(String... arg0) {
            return null;
        }

        protected void onPostExecute(String file_url) {
//			pDialog.dismiss();

            runOnUiThread(new Runnable() {
                              public void run() {
//                                  while (true) {

                                      try {
                                          List<NameValuePair> params = new ArrayList<NameValuePair>();
                                          params.add(new BasicNameValuePair("groupid", groupid));
                                          JSONObject json = jsonParser.makeHttpRequest(LOCATION_URL, "POST", params);
                                          System.out.println(json.toString());
                                          JSONObject jso = new JSONObject();
                                          location = null;
                                          location = new CarLocation();
                                          String status = "";
                                          String longitude = "";
                                          String latitude = "";
                                          String speed = "";
                                          String mileage = "";
                                          String onoff = "";
                                          String time = "";
                                          String fuel = "";
                                          String car_id = "";
                                          String plateNo = "";
                                          CURRENT_LOC = null;
                                          map = null;
                                          map = ((SupportMapFragment) getSupportFragmentManager()
                                                  .findFragmentById(R.id.map)).getMap();
                                          Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                                          for (Iterator it = json.keys(); it.hasNext(); ) {
                                              String key = (String) it.next();
                                              try {
                                                  if (key.equalsIgnoreCase("status")) {
                                                      status = json.getString("status");
                                                  } else {
                                                      JSONObject jsonObject = json.getJSONObject(key);
                                                      longitude = jsonObject.getString("longitude");
                                                      latitude = jsonObject.getString("latitude");
                                                      speed = jsonObject.getString("speed");
                                                      mileage = jsonObject.getString("mileage");
                                                      onoff = jsonObject.getString("status");
                                                      time = jsonObject.getString("gpstime");
                                                      fuel = jsonObject.getString("fuel");
                                                      car_id = jsonObject.getString("carid");
                                                      plateNo = jsonObject.getString("car_name");

                                                      location.setStatus(status);
                                                      location.setLatitude(latitude);
                                                      location.setLongitude(longitude);
                                                      location.setCar_id(car_id);
                                                      location.setOnoff(onoff);
                                                      location.setFuel(fuel);
                                                      String st = "";
                                                      if (!speed.equalsIgnoreCase("0")) {
                                                          st = "ON";
                                                      } else {
                                                          st = "OFF";
                                                      }
                                                      location.setSpeed(speed);
                                                      location.setTime(time);
                                                      location.setMileage(mileage);
                                                      int speedcar = R.drawable.speed_icon;
                                                      int stopcar = R.drawable.stop_icon;
                                                      int icon = 0;
                                                      if (speed.equalsIgnoreCase("0")) {
                                                          icon = stopcar;
                                                      } else {
                                                          icon = speedcar;
                                                      }
                                                      List<Address> addresses = geocoder.getFromLocation(Double.parseDouble(location.getLatitude()) / 1000000.0, Double.parseDouble(location.getLongitude()) / 1000000.0, 1);
                                                      Address obj = addresses.get(0);
                                                      String add = obj.getAddressLine(0);
                                                      System.out.println("in get address " + add + " , " + obj.getSubAdminArea() + ","
                                                              + obj.getAdminArea());
                                                      CURRENT_LOC = new LatLng(Double.parseDouble(location.getLatitude()) / 1000000.0, Double.parseDouble(location.getLongitude()) / 1000000.0);
                                                      map.addMarker(new MarkerOptions().position(CURRENT_LOC)
                                                              .title("Status:" + st + "  Speed: " + speed + " Plate No: " + car_id)
                                                              .snippet("Addres : " + add + " ," + obj.getSubAdminArea() + " ," + obj.getAdminArea())
                                                              .icon(BitmapDescriptorFactory
                                                                      .fromResource(icon)));
//                                                      String reference = jsonObject.getString("carid");
                                                  }
                                              } catch (Exception ex) {
                                                  ex.printStackTrace();
                                              }
                                          }

//                                      location.setStatus(status);
//                                      location.setLatitude(latitude);
//                                      location.setLongitude(longitude);
//                                      location.setCar_id(car_id);
//                                      location.setOnoff(onoff);
//                                      location.setFuel(fuel);
//                                      String st =  "";
//                                      if(!speed.equalsIgnoreCase("0")){
//                                          st = "ON";
//                                      }else{
//                                          st="OFF";
//                                      }
//                                      location.setSpeed(speed);
//                                      location.setTime(time);
//                                      location.setMileage(mileage);
//                                      int speedcar = R.drawable.speed_icon;
//                                      int stopcar = R.drawable.stop_icon;
//                                      int icon =0;
//                                      if (speed.equalsIgnoreCase("0")){
//                                          icon = stopcar;
//                                      }else{
//                                          icon = speedcar;
//                                      }
//                                      CURRENT_LOC = new LatLng(Double.parseDouble(location.getLatitude()) / 1000000.0, Double.parseDouble(location.getLongitude()) / 1000000.0);
//                                      map.addMarker(new MarkerOptions().position(CURRENT_LOC)
//                                              .title("Status:"+st+"  Speed: "+speed +" Plate No: "+plateNo)
//                                              .snippet("")
//                                              .icon(BitmapDescriptorFactory
//                                                      .fromResource(icon)));
//                                          CameraPosition cameraPosition = new CameraPosition.Builder()
//                                                  .target(CURRENT_LOC) // Sets the center of the map to
//                                                          // Golden Gate Bridge
//                                                  .zoom(17)                   // Sets the zoom
//                                                  .bearing(90) // Sets the orientation of the camera to east
//                                                  .tilt(30)    // Sets the tilt of the camera to 30 degrees
//                                                  .build();    // Creates a CameraPosition from the builder
//                                          map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                                      } catch (Exception ex) {
                                          ex.printStackTrace();
                                      }
//                                      long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
//                                      try {
//
//                                          Log.d(Config.TAG, "Sleeping for " + backoff + " ms before retry");
//                                          Thread.sleep(backoff);
//
//                                      } catch (InterruptedException e1) {
//                                          // Activity finished before we complete - exit.
//                                          Log.d(Config.TAG, "Thread interrupted: abort remaining retries!");
//                                          Thread.currentThread().interrupt()
//                                          ;
//                                          return;
//                                      }
//                                      backoff *= 2;
//                                  }

                              }
                          }
            );

        }


    }
}
