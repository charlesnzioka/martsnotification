package belas.net.martsnotification;

/**
 * Created by David on 12/22/2014.
 */
public  class UserData {

    //private variables
    int _id;
    String _imei;
    String _name;
    String _message;
    String _userid;

    // Empty constructor
    public UserData(){

    }
    // constructor
    public UserData(int id, String imei, String name, String message, String userid){
        this._id      = id;
        this._imei    = imei;
        this._name    = name;
        this._message = message;
        this._userid = userid;

    }

    public String get_userid() {
        return _userid;
    }

    public void set_userid(String _userid) {
        this._userid = _userid;
    }

    // getting ID
    public int getID(){
        return this._id;
    }

    // setting id
    public void setID(int id){
        this._id = id;
    }

    // getting imei
    public String getIMEI(){
        return this._imei;
    }

    // setting imei
    public void setIMEI(String imei){
        this._imei = imei;
    }

    // getting name
    public String getName(){
        return this._name;
    }

    // setting name
    public void setName(String name){
        this._name = name;
    }

    // getting Message
    public String getMessage(){
        return this._message;
    }

    // setting Message
    public void setMessage(String message){
        this._message = message;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "UserInfo [name=" + _name + "]";
    }

}