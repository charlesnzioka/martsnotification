package belas.net.martsnotification;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class NotificationReceiver extends BaseActivity {
    private Context context;
    String car_name="", drivername="" , contact1="" ,contact2="", alm_type="", alm_body="", timestamp="";
    Alarm alarm;
    GUIStatics stats ;
    private static final String CAR_ID = "car_id";
    private static final  String ALM_DESC="alm_desc";
    //String carid="" ,
    String alm_desc="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_receiver);
        context =  this;
        Intent i = getIntent();
        //carid=i.getStringExtra(CAR_ID);
        alm_desc= i.getStringExtra(ALM_DESC);
        System.err.print("alm_desc "+alm_desc);
        car_name = alm_desc.split(",")[0];
        drivername= alm_desc.split(",")[3];
        contact1 = alm_desc.split(",")[4];
        contact2 = alm_desc.split(",")[5];
//        alm_body = "";
        alm_type = alm_desc.split(",")[2];
        timestamp = alm_desc.split(",")[6];
        alm_body =  alm_desc.split(",")[7];
        getSupportActionBar().setSubtitle(" Plate No " + car_name);

        // System.out.println("values "+car_id+" , "+longitude+" , "+latitude+" , "+speed+" , "+mileage+" , " + onoff+ " ," +fuel+","+time);
//        new GetLocation().execute(car_id, longitude, latitude, speed, mileage, onoff, fuel, time);


        TextView lbl_carid = (TextView) findViewById(R.id.cardidtxt);
        TextView lbl_address = (TextView) findViewById(R.id.addresstxt);
        TextView lbl_speed = (TextView) findViewById(R.id.speedtxt);
        TextView lbl_fuel = (TextView) findViewById(R.id.fueltxt);
        TextView lbl_status = (TextView) findViewById(R.id.statustext);
        TextView lbl_mileage = (TextView) findViewById(R.id.Mileagetxt);
        TextView lbl_time = (TextView) findViewById(R.id.timetxt);
//        TextView lbl_longitude = (TextView) findViewById(R.id.longitudetxt);
//        TextView lbl_latitude = (TextView) findViewById(R.id.latitidetxt);
//        GUIStatics details =  getAddress(Double.parseDouble(latitude),Double.parseDouble(longitude));
//        System.out.println("Details " +details.getCurrentAddress());
//
//        // Check for null data from google
//        // Sometimes place details might missing
        alarm =  new Alarm();
        alarm.setAlm_body(alm_body);
        alarm.setCar_name(car_name);
//        alarm.setCarid(carid);
        alarm.setDrivername(drivername);
        alarm.setContact1(contact1);
        alarm.setContact2(contact2);
        alarm.setAlm_type(alm_type);
        alarm.setTimestamp(timestamp);

        lbl_carid.setText(car_name);
//        lbl_latitude.setText(latitude);
//        lbl_longitude.setText(longitude);
        lbl_status.setText(alm_body);
        lbl_fuel.setText(alarm.getAlarmDesc(alarm.getAlm_type()));
        lbl_speed.setText(drivername);
        lbl_time.setText(timestamp.replace('T',' '));
        lbl_address.setText(contact1);
//        lbl_mileage.setText(contact2);
//        lbl_status.setText(alm_body);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_notification_receiver, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public GUIStatics getAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        if (geocoder == null){
            System.out.println("Goecoder is null");
        }
        stats =  new GUIStatics();
        if (stats == null){
            System.out.println("stats  is null");
        }
        try {
            System.out.println("in get address lat "+lat+"  longitude "+lng);
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            String add = obj.getAddressLine(0);
            System.out.println("in get address "+add+" , "+obj.getSubAdminArea() + ","
                    + obj.getAdminArea());
            stats.setCurrentAddress( add
                    + ","
                    + obj.getAdminArea() +","+obj.getCountryName());
            stats.setLatitude(String.valueOf(obj.getLatitude()));
            stats.setLongitude(String.valueOf(obj.getLongitude()));
            stats.setCurrentCity(String.valueOf(obj.getSubAdminArea()));
            stats.setCurrentState(obj.getAdminArea());
            add = add + "\n" + obj.getCountryName();
            add = add + "\n" + obj.getCountryCode();
            add = add + "\n" + obj.getAdminArea();
            add = add + "\n" + obj.getPostalCode();
            add = add + "\n" + obj.getSubAdminArea();
            add = add + "\n" + obj.getLocality();
            add = add + "\n" + obj.getSubThoroughfare();

            Log.v("IGA", "Address" + add);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            System.out.println("Error "+e.getMessage());
            e.printStackTrace();
//                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }finally{
            return stats;
        }
    }
}
