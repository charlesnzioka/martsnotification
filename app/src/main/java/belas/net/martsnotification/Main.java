package belas.net.martsnotification;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

//import com.androidexample.mobilegcm.R;
//import com.androidexample.mobilegcm.GridViewExample.LongOperation;
//import com.google.android.gcm.GCMRegistrar;

public class Main extends Activity {

    // label to display gcm messages
    TextView lblMessage;
    Controller aController;
    JSONParser jsonParser = new JSONParser();
    TextView   lbl_text;
    Context context;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /******************* Intialize Database *************/
        DBAdapter.init(this);
        lbl_text = (TextView) findViewById(R.id.info);
        context =  this;

        // Get Global Controller Class object
        // (see application tag in AndroidManifest.xml)
        aController = (Controller) getApplicationContext();


        // Check if Internet present
        if (!aController.isConnectingToInternet()) {

            // Internet Connection is not present
            aController.showAlertDialog(Main.this,
                    "Internet Connection Error",
                    "Please connect to Internet connection", false);
            // stop executing code by return
            return;
        }

        //Check device contains self information in sqlite database or not.
        int vDevice = DBAdapter.validateDevice();

        if(vDevice > 0)
        {

            // Launch Main Activity
            Intent i = new Intent(getApplicationContext(), GroupView.class);
            startActivity(i);
            finish();
        }
        else
        {
            String deviceIMEI = "";
//            if(Config.SECOND_SIMULATOR){
//
//                //Make it true in CONFIG if you want to open second simutor
//                // for testing actually we are using IMEI number to save a unique device
//
//                deviceIMEI = "000000000000001";
//            }
//            else
//            {
                // GET IMEI NUMBER
                TelephonyManager tManager = (TelephonyManager) getBaseContext()
                        .getSystemService(Context.TELEPHONY_SERVICE);
                deviceIMEI = tManager.getDeviceId();
//            }

            /******* Validate device from server ******/
            // WebServer Request URL
            String serverURL = Config.YOUR_SERVER_URL+"validate_user";
            Log.i("GCM Server URL","---"+ serverURL);

            // Use AsyncTask execute Method To Prevent ANR Problem
            LongOperation serverRequest = new LongOperation();

            serverRequest.execute(serverURL,deviceIMEI,"","");

        }

    }


    // Class with extends AsyncTask class
    public class LongOperation  extends AsyncTask<String, Void, String> {

        // Required initialization

        //private final HttpClient Client = new DefaultHttpClient();
        // private Controller aController = null;
        private String Error = null;
        private ProgressDialog Dialog = new ProgressDialog(Main.this);
        String data ="";
        int sizeData = 0;


        protected void onPreExecute() {
            // NOTE: You can call UI Element here.

            //Start Progress Dialog (Message)

            Dialog.setMessage("Validating Device..");
            Dialog.show();

        }

        // Call after onPreExecute method
        protected String doInBackground(String... params) {

            /************ Make Post Call To Web Server ***********/
            BufferedReader reader=null;
            JSONObject Content = null;
            // Send data
            try{

                List<NameValuePair> items = new ArrayList<NameValuePair>();
                items.add(new BasicNameValuePair("imei", params[1].toString()));
                JSONObject json = jsonParser.makeHttpRequest(params[0], "POST", items);
                System.out.println(json.toString());
                JSONObject jso = new JSONObject();

                // Defined URL  where to send data
                URL url = new URL(params[0]);

                // Set Request parameter
                if(!params[1].equals(""))
                    data +="&" + URLEncoder.encode("imei", "UTF-8") + "="+params[1].toString();
                if(!params[2].equals(""))
                    data +="&" + URLEncoder.encode("data2", "UTF-8") + "="+params[2].toString();
                if(!params[3].equals(""))
                    data +="&" + URLEncoder.encode("data3", "UTF-8") + "="+params[3].toString();
                Log.i("GCM",data);

                // Send POST data request
                System.out.println("URL "+data);

                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( data );
                wr.flush();

                // Get the server response

                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while((line = reader.readLine()) != null)
                {
                    // Append server response in string
                    sb.append(line + " ");
                }

                // Append Server Response To Content String
              // Content = sb.toString();
                Content =json;
                Log.i("Data from  the server ",Content.toString());

            }
            catch(Exception ex)
            {
                Error = ex.getMessage();
            }
            finally
            {
                try
                {

                    reader.close();
                }

                catch(Exception ex) {}
            }

            /*****************************************************/
            return Content.toString();
        }

        protected void onPostExecute(String Content) {
            // NOTE: You can call UI Element here.

            // Close progress dialog
            Dialog.dismiss();

            if (Error != null) {


            } else {

                // Show Response Json On Screen (activity)

                /****************** Start Parse Response JSON Data *************/
                aController.clearUserData();

                JSONObject jsonMain;

                try {

                    jsonMain =    new JSONObject(Content);
                    String Status ="";

                    for (Iterator it = jsonMain.keys(); it.hasNext(); ) {
                        String key = (String) it.next();
//							                  System.out.println("Key "+key);


                        if (key.equalsIgnoreCase("status")) {
                            Status = jsonMain.getString("status");
                        } else {
                            if (key.equalsIgnoreCase("Android")) {
                                JSONObject json = jsonMain.getJSONObject(key);
                                //Log.i("GCM Status","---"+Sta);
                                Status = json.getString("Status");
                                Log.i("GCM Status","---"+Status);
                                Log.i("GCM:: JSON response ","---"+json.toString());

                                // IF server response status is update
                                if(Status.equals("update")){

                                    String RegID      = json.optString("regid").toString();
                                    String Name       = json.optString("name").toString();
                                    String Email      = json.optString("email").toString();
                                    String IMEI       = json.optString("imei").toString();
                                    String userid   = json.optString("userid").toString();

                                    // add device self data in sqlite database
                                    DBAdapter.addDeviceData(Name, Email,RegID, IMEI,userid);

                                    // Launch GridViewExample Activity
                                    Intent i1 = new Intent(getApplicationContext(), GroupView.class);
                                    i1.putExtra("groupid",userid);
                                    startActivity(i1);
                                    finish();

                                    //Log.i("GCM","---"+Name);
                                }
                                else if(Status.equals("install")){

                                    // IF server response status is install

                                    // Launch RegisterActivity Activity
                                    Intent i1 = new Intent(getApplicationContext(), RegisterActivity.class);
                                    startActivity(i1);
                                    finish();

                                }else if  (Status.equalsIgnoreCase("denied")){
                                    Log.i("GCM ","Status denied.  Updateing tesxt field.");
                                    lbl_text.setText("Access Denied.  Please consult Marts logic");
                                    //displayRegistrationMessageOnScreen(context, "Access Denied.  Please consult Marts logic");
                                    //userid ="Access Denied.  Please consult Marts logic";

                                }

                            }

                        }

                    }
//                    // Creates a new JSONObject with name/value mappings from the JSON string.
//                    jsonResponse
//
//                    // Returns the value mapped by name if it exists and is a JSONArray.
//                    // Returns null otherwise.
//                    JSONArray jsonMainNode = jsonResponse.optJSONArray("Android");
//                    System.out.println(jsonMainNode.toString());
//
//                    /*********** Process each JSON Node ************/
//
//                    int lengthJsonArr = jsonMainNode.length();
//
//                    for(int i=0; i < lengthJsonArr; i++)
//                    {
                        /****** Get Object for each JSON node.***********/
//                        JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);


                        /******* Fetch node values **********/
                       // String Status       = jsonChildNode.optString("status").toString();





//                    }

                    /****************** End Parse Response JSON Data *************/


                } catch (JSONException e) {

                    e.printStackTrace();
                }



            }
        }

    }
    void displayRegistrationMessageOnScreen(Context context, String message) {

        Intent intent = new Intent(Config.DISPLAY_MESSAGE_ACTION);
        intent.putExtra(Config.EXTRA_MESSAGE, message);

        // Send Broadcast to Broadcast receiver with message
        context.sendBroadcast(intent);

    }




    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

}