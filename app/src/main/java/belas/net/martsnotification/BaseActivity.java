package belas.net.martsnotification;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

/**
 * Created by Charles on 2/28/15.
 */
public class BaseActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }
}
