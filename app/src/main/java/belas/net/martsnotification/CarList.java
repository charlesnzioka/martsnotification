package belas.net.martsnotification;

/**
 * Created by David on 12/26/2014.
 */
import java.io.Serializable;
import java.util.List;


/**
 * Created by David on 11/24/2014.
 */
public class CarList implements Serializable {

    private String status;


    private List<Car> results;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Car> getResults() {
        return results;
    }

    public void setResults(List<Car> results) {
        this.results = results;
    }
}
