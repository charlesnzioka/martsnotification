package belas.net.martsnotification;

/**
 * Created by David on 12/26/2014.
 */

import java.io.Serializable;

/**
 * Created by David on 11/24/2014.
 */
public class Car  implements Serializable {

    public String plateNo;
    public String reference;
    public String ignition;
    public String speed;

    public String getIgnition() {
        return ignition;
    }

    public void setIgnition(String ignition) {
        this.ignition = ignition;
    }

    public String getPlateNo() {
        return plateNo;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public void setPlateNo(String plateNo) {
        this.plateNo = plateNo;
    }

    public String getReference() {
        return reference;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

}
