package belas.net.martsnotification;

/**
 * Created by David on 12/28/2014.
 */

public class Alarm {

    private String carid;
    private String contact1;
    private String contact2;
    private String alm_body;
    private String alm_type;
    private String timestamp;
    private String car_name;
    private String drivername;


    public String getCarid() {
        return carid;
    }

    public void setCarid(String carid) {
        this.carid = carid;
    }

    public String getContact1() {
        return contact1;
    }

    public void setContact1(String contact1) {
        this.contact1 = contact1;
    }

    public String getContact2() {
        return contact2;
    }

    public void setContact2(String contact2) {
        this.contact2 = contact2;
    }

    public String getAlm_body() {
        return alm_body;
    }

    public void setAlm_body(String alm_body) {
        this.alm_body = alm_body;
    }

    public String getAlm_type() {
        return alm_type;
    }

    public void setAlm_type(String alm_type) {
        this.alm_type = alm_type;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getCar_name() {
        return car_name;
    }

    public void setCar_name(String car_name) {
        this.car_name = car_name;
    }

    public String getDrivername() {
        return drivername;
    }

    public void setDrivername(String drivername) {
        this.drivername = drivername;
    }

    public String getAlarmDesc(String alam) {
        alam = alam.trim();
        if (alam.equals("1")) {
            return "enter geo-fence";
        } else if (alam.equals("2")) {
            return "out geo-fence";
        } else if (alam.equals("3")) {
            return "overtime parking alarm";
        } else if (alam.equals("4")) {
            return "GSM module reset alarm";
        } else if (alam.equals("5")) {
            return "fatigue driving alarm";
        } else if (alam.equals("6")) {
            return "Over speed alarm";
        } else if (alam.equals("7")) {
            return "SOS alarm";
        } else if (alam.equals("8")) {
            return "Using backup battery alarm";
        } else if (alam.equals("9")) {
            return "Illegal open door alarm";
        } else if (alam.equals("10")) {
            return "Illegal ignition on alarm";
        } else if (alam.equals("11")) {
            return "fuel level change alarm";
        } else if (alam.equals("12")) {
            return "tow alarm";
        } else if (alam.equals("13")) {
            return "Vibration alarm";
        } else if (alam.equals("14")) {
            return "hot spot in/out alarm";
        } else if (alam.equals("15")) {
            return "Zone or road out/in alarm";
        } else if (alam.equals("16")) {
            return "Zone or road over speed and overtime parking alarm";
        } else if (alam.equals("17")) {
            return "Zone or road time relative alarm, such as reach to one road at a forbidden time";
        } else if (alam.equals("21")) {
            return "";
        } else if (alam.equals("20")) {
            return "";
        } else if (alam.equals("18")) {
            return "Extend digital fuel level change alarm";
        } else if (alam.equals("19")) {
            return "Universal port pulse calculation alarm";
        } else if (alam.equals("22")) {
            return "Temperature change alarm";
        } else if (alam.equals("23")) {
            return "Seat belt unfasten alarm";
        } else if (alam.equals("24")) {
            return "Harsh braking alarm";
        } else if (alam.equals("25")) {
            return "Neutral gear sliding alarm";
        } else if (alam.equals("26")) {
            return "Tyre pressure alarm";
        } else if (alam.equals("27")) {
            return "GPRS connection Reconnect alarm";
        } else if (alam.equals("28")) {
            return "GPS antenna disconnected alarm";

        } else if (alam.equals("1000")) {
            return "General alarm";
        } else{
            return "";

        }

    }

}