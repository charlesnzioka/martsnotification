package belas.net.martsnotification;


import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Random;


public class MapsActivity extends BaseActivity {

    GoogleMap map;
    MapView mapView;
    String  carid;
    CarLocation location;
    JSONParser jsonParser = new JSONParser();
    private static final String LOCATION_URL = Config.YOUR_SERVER_URL+"getCarLocation";
    private static final String CAR_ID = "car_id";
    String plateNo;
    private  final int BACKOFF_MILLI_SECONDS = 64*1000;
    private  final Random random = new Random();
    long backoff = BACKOFF_MILLI_SECONDS;

    LatLng CURRENT_LOC ;
    //    = new LatLng(37.828891,-122.485884);
    private static final LatLng APPLE =   new LatLng(37.3325004578, -122.03099823);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        Intent i = getIntent();
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        // Place referece id
        carid = i.getStringExtra(CAR_ID);
        plateNo=  i.getStringExtra("plateNo");




        map = ((SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map)).getMap();
        if (map == null) {
            Toast.makeText(this, "Google Maps not available",
                    Toast.LENGTH_LONG).show();
        }
        new  GetLocation().execute();
        getSupportActionBar().setSubtitle(" Plate No::" + plateNo);
//        if( location.getStatus().equalsIgnoreCase("OK")) {



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is
        // present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.menu_sethybrid:
                map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                break;
            case R.id.menu_show_details:
                Intent in = new Intent(getApplicationContext(), ViewDetails.class);
                in.putExtra(CAR_ID, carid);
                in.putExtra("plateNo",plateNo);
                in.putExtra("longitude",location.getLongitude());
                in.putExtra("latitude",location.getLatitude());
                in.putExtra("speed",location.getSpeed());
                in.putExtra("mileage",location.getMileage());
                in.putExtra("fuel",location.getFuel());
                in.putExtra("time",location.getTime());
                in.putExtra("status",location.getOnoff());
                startActivity(in);
                break;


        }
        return true;
    }


    class GetLocation extends AsyncTask<String, String, String> {


        protected void onPreExecute() {
            super.onPreExecute();
//            pDialog = new ProgressDialog(ShowCars.this);
//            pDialog.setMessage(Html.fromHtml("<b>Updating Database</b><br/>Loading Available Cars..."));
//            pDialog.setIndeterminate(false);
//            pDialog.setCancelable(false);
//            pDialog.show();
//        }
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("carid", carid));
                JSONObject json = jsonParser.makeHttpRequest(LOCATION_URL, "POST", params);
                System.out.println(json.toString());
                JSONObject jso = new JSONObject();
                location = null;
                location = new CarLocation();
                String status = "";
                String longitude = "";
                String latitude = "";
                String speed = "";
                String mileage = "";
                String onoff = "";
                String time = "";
                String fuel = "";
                String car_id = "";
                for (Iterator it = json.keys(); it.hasNext(); ) {
                    String key = (String) it.next();
                    try {
                        if (key.equalsIgnoreCase("status")) {
                            status = json.getString("status");
                        } else {
                            JSONObject jsonObject = json.getJSONObject(key);
                            longitude = jsonObject.getString("longitude");
                            latitude = jsonObject.getString("latitude");
                            speed = jsonObject.getString("speed");
                            mileage = jsonObject.getString("mileage");
                            onoff = jsonObject.getString("status");
                            time = jsonObject.getString("gpstime");
                            fuel = jsonObject.getString("fuel");
                            car_id = jsonObject.getString("carid");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                CURRENT_LOC = null;
                map = null;
                map = ((SupportMapFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.map)).getMap();
                location.setStatus(status);
                location.setLatitude(latitude);
                location.setLongitude(longitude);
                location.setCar_id(car_id);
                location.setOnoff(onoff);
                location.setFuel(fuel);
                location.setSpeed(speed);
                location.setTime(time);
                location.setMileage(mileage);



//                                               count = 1;
//                                               //Thread.sleep(backoff);
//                                           }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String file_url) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int speedcar = R.drawable.speed_icon;
                    int stopcar = R.drawable.stop_icon;
                    int icon = 0;
                    if (location.getSpeed().equalsIgnoreCase("0")) {
                        icon = stopcar;
                    } else {
                        icon = speedcar;
                    }
                    String st = "";
                    if (!location.getSpeed().equalsIgnoreCase("0")) {
                        st = "ON";
                    } else {
                        st = "OFF";
                    }
                    CURRENT_LOC = new LatLng(Double.parseDouble(location.getLatitude()) / 1000000.0, Double.parseDouble(location.getLongitude()) / 1000000.0);

                    map.addMarker(new MarkerOptions().position(CURRENT_LOC)
                            .title("Status:" + st + "  Speed: " + location.getSpeed() + " Plate No: " + plateNo)
                            .snippet("")
                            .icon(BitmapDescriptorFactory
                                    .fromResource(icon)));

                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(CURRENT_LOC) // Sets the center of the map to
                                    // Golden Gate Bridge
                            .zoom(17)                   // Sets the zoom
                            .bearing(90) // Sets the orientation of the camera to east
                            .tilt(30)    // Sets the tilt of the camera to 30 degrees
                            .build();    // Creates a CameraPosition from the builder
                    map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                }
            });

        }


    }

}