package belas.net.martsnotification;

/**
 * Created by David on 12/22/2014.
 */
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DBAdapter {

    /***** if debug is set true then it will show all Logcat message ****/
    public static final boolean DEBUG = true;

    /******************** Logcat TAG ************/
    public static final String LOG_TAG = "DBAdapter";

    /******************** Table Fields ************/
    public static final String KEY_ID = "_id";

    public static final String KEY_USER_IMEI    = "user_imei";

    public static final String KEY_USER_NAME    = "user_name";

    public static final String KEY_USER_MESSAGE = "user_message";

    public static final String KEY_DEVICE_IMEI  = "device_imei";

    public static final String KEY_DEVICE_NAME  = "device_name";

    public static final String KEY_DEVICE_EMAIL = "device_email";

    public static final String KEY_DEVICE_REGID = "device_regid";

    public static final String KEY_DEVICE_CREATEAT = "created_at";

    public static final String KEY_DEVICE_USERID = "userid";



    /******************** Database Name ************/
    public static final String DATABASE_NAME = "DB_sqllite";

    /**** Database Version (Increase one if want to also upgrade your database) ****/
    public static final int DATABASE_VERSION = 1;// started at 1

    /** Table names */
    public static final String USER_TABLE = "tbl_user";
    public static final String DEVICE_TABLE = "tbl_device";

    /*** Set all table with comma seperated like USER_TABLE,ABC_TABLE ***/
    private static final String[] ALL_TABLES = { USER_TABLE,DEVICE_TABLE };

    /** Create table syntax */

    private static final String USER_CREATE =
            "create table tbl_user(_id integer primary key autoincrement,  user_name text not null," +
                    "    user_imei text not null,    user_message text not null, userid  text not null, created_at DATETIME DEFAULT CURRENT_TIMESTAMP);";

    private static final String DEVICE_CREATE =
            "create table tbl_device(_id integer primary key autoincrement, " +
                    "   device_name text not null, device_email text not null, device_regid text not null, device_imei text not null, userid text not null);";

    /**** Used to open database in syncronized way ****/
    private static DataBaseHelper DBHelper = null;

    private static  String DELETESQL= "DELETE FROM "+USER_TABLE+" WHERE created_at < ";

    protected DBAdapter() {
    }

    /******************* Initialize database *************/
    public static void init(Context context) {
        if (DBHelper == null) {
            if (DEBUG)
                Log.i("DBAdapter", context.toString());
            DBHelper = new DataBaseHelper(context);
        }
    }

    /***** Main Database creation INNER class ******/
    private static class DataBaseHelper extends SQLiteOpenHelper {
        public DataBaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            if (DEBUG)
                Log.i(LOG_TAG, "new create");
            try {
                //db.execSQL(USER_MAIN_CREATE);
                db.execSQL(USER_CREATE);
                db.execSQL(DEVICE_CREATE);

            } catch (Exception exception) {
                if (DEBUG)
                    Log.i(LOG_TAG, "Exception onCreate() exception");
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            if (DEBUG)
                Log.w(LOG_TAG, "Upgrading database from version" + oldVersion
                        + "to" + newVersion + "...");

            for (String table : ALL_TABLES) {
                db.execSQL("DROP TABLE IF EXISTS " + table);
            }
            onCreate(db);
        }

    } // Inner class closed


    /**** Open database for insert,update,delete in syncronized manner ****/
    private static synchronized SQLiteDatabase open() throws SQLException {
        return DBHelper.getWritableDatabase();
    }




    // Insert installing device data
    public static void addDeviceData(String DeviceName, String DeviceEmail,
                                     String DeviceRegID,String DeviceIMEI, String DEVICEUSERID)
    {
        try{
            final SQLiteDatabase db = open();

            String imei  = sqlEscapeString(DeviceIMEI);
            String name  = sqlEscapeString(DeviceName);
            String email = sqlEscapeString(DeviceEmail);
            String regid = sqlEscapeString(DeviceRegID);
            String userid =  sqlEscapeString(DEVICEUSERID);

            ContentValues cVal = new ContentValues();
            cVal.put(KEY_DEVICE_IMEI, imei);
            cVal.put(KEY_DEVICE_NAME, name);
            cVal.put(KEY_DEVICE_EMAIL, email);
            cVal.put(KEY_DEVICE_REGID, regid);
            cVal.put(KEY_DEVICE_USERID, userid);


            db.insert(DEVICE_TABLE, null, cVal);
            db.close(); // Closing database connection
        } catch (Throwable t) {
            Log.i("Database", "Exception caught: " + t.getMessage(), t);
        }
    }


    // Adding new user

    public static void addUserData(UserData uData) {
        try{
            final SQLiteDatabase db = open();

            String imei  = sqlEscapeString(uData.getIMEI());
            String name  = sqlEscapeString(uData.getName());
            String message  = sqlEscapeString(uData.getMessage());
            String userid = sqlEscapeString(uData.get_userid());


            ContentValues cVal = new ContentValues();
            cVal.put(KEY_USER_IMEI, imei);
            cVal.put(KEY_USER_NAME, name);
            cVal.put(KEY_USER_MESSAGE, message);
            cVal.put(KEY_DEVICE_USERID,userid);
            cVal.put(KEY_DEVICE_CREATEAT,getDateTime());

            db.insert(USER_TABLE, null, cVal);
            db.close(); // Closing database connection
        } catch (Throwable t) {
            Log.i("Database", "Exception caught: " + t.getMessage(), t);
        }



    }

    public static boolean deleteUserDate(){
        final SQLiteDatabase db = open();
        System.out.println("TIME server "+get5Minutes());
        return  db.delete(USER_TABLE, KEY_DEVICE_CREATEAT +"<'"+ get5Minutes()+"'", null)> 0;

    }




    private static  String get5Minutes(){
        long ONE_MINUTE_IN_MILLIS= 1000;//millisecs
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        Date date = new Date();
        long t=date.getTime();

        Date afterAddingTenMins=new Date(t - (10*60* ONE_MINUTE_IN_MILLIS));
        return dateFormat.format(afterAddingTenMins);
    }

    private static  String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    // Getting single user data
    public static UserData getUserData(int id) {
        final SQLiteDatabase db = open();

        Cursor cursor = db.query(USER_TABLE, new String[] { KEY_ID,
                        KEY_USER_NAME, KEY_USER_IMEI,KEY_USER_MESSAGE,KEY_DEVICE_USERID}, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        UserData data = new UserData(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
        // return contact
        return data;
    }

    public static UserData getUserData() {
        Log.i("GCM: ", "Getting USER DETAILS");
//        final SQLiteDatabase db = open();
//        String selectQuery =  "SELECT  * FROM "+DEVICE_TABLE;
        UserData data= null;
        int count =0 ;
        try {
            String countQuery = "SELECT *   FROM " + USER_TABLE ;
            final SQLiteDatabase db = open();
            Cursor cursor = db.rawQuery(countQuery, null);
            count = cursor.getCount();
            Log.i("Database", "Number of records: " + count);
            if (cursor != null)
                cursor.moveToFirst();
             data = new UserData(Integer.parseInt(cursor.getString(0)),
                    cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));


            cursor.close();

        } catch (Throwable t) {
            count = 10;
            Log.i("Database", "Exception caught: " + t.getMessage(), t);
        }finally {
            return  data;
        }

//        Cursor cursor = db.query(USER_TABLE, new String[] { KEY_ID,
//                        KEY_USER_NAME, KEY_USER_IMEI,KEY_USER_MESSAGE,KEY_DEVICE_USERID}, KEY_ID
//                , null, null, null, null);

//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        if (cursor != null)
//            cursor.moveToFirst();
//
//        UserData data = new UserData(Integer.parseInt(cursor.getString(0)),
//                cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
//
//
//        // return contact
//        return data;
    }

    // Getting All user data
    public static List<UserData> getAllUserData() {
        List<UserData> contactList = new ArrayList<UserData>();
        // Select All Query


        String selectQuery = "SELECT  * FROM " + USER_TABLE+" ORDER BY "+KEY_ID+" desc";

        final SQLiteDatabase db = open();
        Cursor cursor = db.rawQuery(selectQuery, null);

        int count = 0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                UserData data = new UserData();
                count++;
                data.setID(Integer.parseInt(cursor.getString(0)));
                data.setName(cursor.getString(1));
                data.setIMEI(cursor.getString(2));
                data.setMessage(cursor.getString(3));
                Log.i("Database", "Query : " + cursor.getString(4) + " Message "+data.getMessage() );
                // Adding contact to list
                contactList.add(data);
            } while (cursor.moveToNext());
        }
        Log.i("Database", "Count of Records : "+count );
        cursor.close();
        // return contact list
        return contactList;
    }

    // Getting users Count
    public static int getUserDataCount() {
        String countQuery = "SELECT  * FROM " + USER_TABLE;
        final SQLiteDatabase db = open();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }

    // Getting installed device have self data or not
    public static int validateDevice() {
        String countQuery = "SELECT  * FROM " + DEVICE_TABLE;
        final SQLiteDatabase db = open();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();
        Log.i("Database", "Devices Count: " + count);

        // return count
        return count;
    }

    // Getting distinct user data use in spinner
    public static List<UserData> getDistinctUser() {
        List<UserData> contactList = new ArrayList<UserData>();
        // Select All Query
        String selectQuery = "SELECT  distinct(user_imei),user_name   FROM " + USER_TABLE+"     ORDER BY "+KEY_ID+" desc";

        final SQLiteDatabase db = open();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                UserData data = new UserData();

                data.setIMEI(cursor.getString(0));
                data.setName(cursor.getString(1));

                // Adding contact to list
                contactList.add(data);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return contactList;
    }

    // Getting imei already in user table or not
    public static int validateNewMessageUserData(String IMEI) {
        int count = 0;
        try {
            String countQuery = "SELECT "+KEY_ID+"   FROM " + USER_TABLE + "     WHERE user_imei='"+IMEI+"'";

            final SQLiteDatabase db = open();
            Cursor cursor = db.rawQuery(countQuery, null);

            count = cursor.getCount();
            Log.i("Database", "Count of records: " + count);
            cursor.close();
        } catch (Throwable t) {
            count = 10;
            Log.i("Database", "Exception caught: " + t.getMessage(), t);
        }
        return count;
    }

    public static String getUserId(String IMEI) {
        int count = 0;
        String userid= "";
        try {
            String countQuery = "SELECT "+KEY_DEVICE_USERID +"  FROM " + DEVICE_TABLE  + "     WHERE device_imei like '"+IMEI+"'";
            Log.i("Database", "Query : " +countQuery );
            final SQLiteDatabase db = open();
            Cursor cursor = db.rawQuery(countQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    UserData data = new UserData();

                    userid= cursor.getString(0);
                    Log.i("Database", "Query results  : " + userid + " Records "+cursor.getCount());
                    // Adding contact to list
                } while (cursor.moveToNext());
            }

            cursor.close();
        } catch (Throwable t) {
            count = 10;
            Log.i("Database", "Exception caught: " + t.getMessage(), t);
        }
        return userid;
    }


    // Escape string for single quotes (Insert,Update)
    private static String sqlEscapeString(String aString) {
        String aReturn = "";

        if (null != aString) {
            //aReturn = aString.replace("'", "''");
            aReturn = DatabaseUtils.sqlEscapeString(aString);
            // Remove the enclosing single quotes ...
            aReturn = aReturn.substring(1, aReturn.length() - 1);
        }

        return aReturn;
    }
    // UnEscape string for single quotes (show data)
    private static String sqlUnEscapeString(String aString) {

        String aReturn = "";

        if (null != aString) {
            aReturn = aString.replace("''", "'");
        }

        return aReturn;
    }
}
