package belas.net.martsnotification.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import belas.net.martsnotification.R;
import belas.net.martsnotification.ShowCars;

/**
 * Created by user on 3/1/15.
 */

public class CarListAdapter extends SimpleAdapter {
    Context context;
    List data;
    int resource;
    static class ViewHolder {
        public TextView reference;
        public TextView name;
        public ImageView image;
    }
    /**
     * Constructor
     *
     * @param context  The context where the View associated with this SimpleAdapter is running
     * @param data     A List of Maps. Each entry in the List corresponds to one row in the list. The
     *                 Maps contain the data for each row, and should include all the entries specified in
     *                 "from"
     * @param resource Resource identifier of a view layout that defines the views for this list
     *                 item. The layout file should include at least those named views defined in "to"
     * @param from     A list of column names that will be added to the Map associated with each
     *                 item.
     * @param to       The views that should display column in the "from" parameter. These should all be
     *                 TextViews. The first N views in this list are given the values of the first N columns
     */
    public CarListAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
        super(context, data, resource, from, to);
        this.context = context;
        this.data = data;
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        // reuse views
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(this.resource, null);
            // configure view holder
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.reference = (TextView) rowView.findViewById(R.id.reference);
            viewHolder.name = (TextView) rowView.findViewById(R.id.name);
            viewHolder.image = (ImageView) rowView
                    .findViewById(R.id.imgCoverArt);
            rowView.setTag(viewHolder);
        }
        //fill data
        ViewHolder holder = (ViewHolder) rowView.getTag();
        HashMap<String, String> carItem = (HashMap<String, String>) data.get(position);
        Double speed = Double.parseDouble(carItem.get(ShowCars.KEY_SPEED));
        if (speed == 0.0) {
            holder.image.setImageResource(R.drawable.stop_icon);
        } else {
            holder.image.setImageResource(R.drawable.speed_icon);
        }
        holder.reference.setText(carItem.get(ShowCars.KEY_NAME));
        holder.name.setText(carItem.get(ShowCars.KEY_REFERENCE));
        return rowView;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }
}
